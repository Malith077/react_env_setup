'use strict'

import React from 'react';



class about extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <>
                <div className="about-me">
                    <div class="card text-center">
                        <div class="card-header">
                            <h3>About Me</h3>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Hi! I'm Malith.</h5>
                            <p class="card-text">An individual with a keen interest in Software programs, possessing good interpersonal and analytical skills, who wish to use technical knowledge catering to the needs of the company. I wish to work with a diverse group of people and see myself heading a group of people.  </p>
                            <ul>
                                <li><a href="/doc/Resume_Malith .pdf" class="btn btn-primary" style={{marginBottom : '10px'}} download>Download my resume</a> <br/> </li>
                                <li><a href="https://github.com/Malith077" class="btn btn-primary" style={{marginBottom : '10px'}}><span style={{ fontSize: "20px", paddingLeft: "2px", paddingRight: "2px" }}><i class="fab fa-github"></i> </span> Github</a> <br/> </li>
                                <li><a href="https://gitlab.com/Malith077" class="btn btn-primary" style={{marginBottom : '10px'}}><span style={{ fontSize: "20px", paddingLeft: "2px", paddingRight: "2px" }}><i class="fab fa-gitlab"></i> </span> GtiLab</a> <br/> </li>
                            </ul>                            
                        </div>
                        <div class="card-footer text-muted">
                            Nice to meet you!
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default about;