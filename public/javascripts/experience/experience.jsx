'use strict'

import React from 'react';
import ReactPlayer from "react-player";


class experience extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <>
        <div className="col-sm-12 Second-column" style={{ paddingTop: '20px' }}>
          <div className="work-experience">
            <h2><span class="material-icons">computer</span> Work Experience</h2>
            <hr />

            <div>
              <ul>
                <li><h4>C++ Developer - Constraint Technologies International</h4></li>
                <li><h5>Melbourne</h5></li>
                <li><span className="badge badge-light">2019/05</span> - <span className="badge badge-light">Current</span></li>
                <li>
                  <div className="row">
                    <div className="col-sm-2">
                      <img src="/images/cti.png" width="200" height="200" alt="" />
                    </div>
                    <div className="col-sm-10">
                      <ul>
                        <li>C++/SQL and OpenGL for developing software for Terminal Management and Airport management </li>
                        <li>Agile/Scrum and Waterfall methodologies </li>
                        <li>Developing and Testing Terminal Management System for Hong Kong, Gatwick Airports </li>
                        <li>Design new features on client demands </li>
                      </ul>
                    </div>
                  </div>

                </li>
              </ul>
              <hr />
            </div>

            <div>

              <ul>
                <li><h4>React Developer - Adilam Technologies</h4></li>
                <li><h5>Bayswater, Melbourne</h5></li>
                <li><span className="badge badge-light">2018/12</span> - <span className="badge badge-light">2019/12</span></li>
                <li>
                  <div className="row">
                    <div className='col-sm-2'>
                      <img src="/images/adilam.png" width="200" height="200" alt="" />
                    </div>
                    <div className='col-sm-10'>
                      <ul>
                        <li>JavaScript, MEAN (Mongo, ExpressJS, AngularJS,NodeJs) and MERN (Mongo, ExpressJS, React, NodeJS) along with Redux in order to create an online life story system <a href="https://ourstoryshared.com/">ourstoryshares.com</a> </li>
                        <li>Agile and Waterfall methodologies </li>
                        <li>Design UI/UX designs </li>
                        <li>Develop and test new features on the system </li>
                        <li>Carry out user tests and document findings </li>
                      </ul>
                    </div>
                  </div>

                </li>
              </ul>
              <hr />
            </div>

            <div>
              <ul>
                <li><h4>React/MEAN Developer - Fantipper.com</h4></li>
                <li><h5>Fitzroy, Melbourne</h5></li>
                <li><span className="badge badge-light">2017/11</span> - <span className="badge badge-light">2019/12</span></li>
                <li>
                  <div className="row">
                    <div className="col-sm-2">
                      <img src="/images/fantipper.png" width="200" height="200" alt="" />
                    </div>
                    <div className="col-sm-10">
                      <ul>
                        <li>MEAN Stack, React and Redux technologies used in creating <a href="https://fantipper.com.au/">fantipper.com</a> </li>
                        <li>React Native is being used for the Fantipper mobile application (Cross-platform) </li>
                        <li>Agile and Waterfall methodologies </li>
                        <li>Implementing a payment platform in order to collect the tips made by users ( Stripe ) </li>
                        <li>UX/UI design and user testing </li>
                      </ul>
                    </div>
                  </div>

                </li>
              </ul>
              <hr />
            </div>

          </div>

          <div className="education">
            <div>
              <ul>
                <li><h2><span class="material-icons">school</span> Education </h2></li>
                <li><h4>Bachelor of Computer Science - Software Developemt</h4></li>
                <ul>
                  <li>Swinburne University of Technology - Melbourne, Australia</li>
                  <li><span className="badge badge-light">2015/08</span> - <span className="badge badge-light">2019/12</span></li>
                </ul>
              </ul>
              <hr />
            </div>

            <div>

              <ul>
                <li><h3><span className="material-icons">code</span> Degree Related Projects</h3></li>
                <li><h5>Working for ourstoryshared.com at Adilam Technologies as a student for the final year Industry Based Learning (IBL) project units. Achived skills are:</h5></li>
                <li>
                  <ul>
                    <li>JavaScript, MEAN (Mongo, ExpressJS, AngularJS, NodeJs) and MERN (Mongo, ExpressJS, React, NodeJS) along with Redux in order to create an online life story system ourstoryshares.com</li>
                    <li>Agile and Waterfall methodologies </li>
                    <li>Design UI/UX designs </li>
                    <li>Develop and test new features on the system </li>
                    <li>Carry out user tests and document findings</li>
                  </ul>
                </li>
              </ul>
            </div>

            <div>
              <ul>
                <li><h3> <span className="material-icons">emoji_events</span>Awards and Achievements</h3></li>
                <li><p>Swinburne International Excellence Scholarship (2015) </p></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><h3><span class="material-icons">show_chart</span>Navitas Professional year coursework</h3></li>
                <li><p>Interview Innovation</p></li>
                <li>
                  <ul>
                    <li>
                      <iframe src="https://drive.google.com/file/d/1Fi-79Y2Ilk7lBkrcwQWyiS7Cd21TJS3_/preview" width="640" height="480"></iframe>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>

            <div>
              <ul>
                <li><h3><span class="material-icons">web</span>Blogs</h3></li>
                <li>
                  <ul>
                    <li><a href="https://industryreportronoreindustry.blogspot.com/" target="_blank">INDUSTRY REPORT - IRON ORE INDUSTRY IN AUSTRALIA</a></li>
                    <li><a href="https://promotionalstrategiesn.blogspot.com/" target="_blank">Promotional Strategies</a></li>
                    <li><a href="https://workplacdiscrimination1.blogspot.com/" target="_blank">Workplace Discrimination</a></li>
                    <li><a href="https://seekingpromotion1.blogspot.com/" target="_blank">Seeking a promotion</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>



        </div>
      </>
    )
  }
}

export default experience;