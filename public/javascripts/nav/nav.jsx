'use strict'
import ReactTooltip from 'react-tooltip';

import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';


class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <>
        <Navbar color="light" style={{ backgroundColor: '#ebe7e7' }} light expand="md">
          <NavbarBrand href="/profile">Prabuddha Malith </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/profile"> <i class="material-icons" data-tip data-for="profile">
                  account_circle</i>
                  <ReactTooltip id="profile" place="top" effect="solid">
                    Profile
                  </ReactTooltip>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/about"> <i class="material-icons" data-tip data-for="aboutme">
                  content_paste</i>
                  <ReactTooltip id="aboutme" place="top" effect="solid">
                    About me
                  </ReactTooltip>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/experience"> <i class="material-icons" data-tip data-for="expnedu">
                  timeline</i>
                  <ReactTooltip id="expnedu" place="top" effect="solid">
                    Education and Experience
                  </ReactTooltip>
                </NavLink>
              </NavItem>
            </Nav>

          </Collapse>
        </Navbar>
      </>
    )
  }
}

export default NavigationBar;