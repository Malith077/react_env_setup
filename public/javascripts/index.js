"use strict"

require("babel-polyfill");

import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap'

//Page
import Profile from './profile/profile.jsx';
import NavigationBar from './nav/nav.jsx';
import About from './about/about.jsx';
import Experience from './experience/experience.jsx';

//Mount target
const profileDiv = document.getElementById('profile-div');
const navLocation = document.getElementById('navbar-location');
const aboutLocation = document.getElementById('about-div');
const experience = document.getElementById('exp-div');

if (profileDiv) {
    ReactDOM.render(
        <Profile />,
        profileDiv
    );
}

if (navLocation) {
    ReactDOM.render(
        <NavigationBar />,
        navLocation
    )
}


if (aboutLocation) {
    ReactDOM.render(
        <About />,
        aboutLocation
    );
}

if (experience) {
    ReactDOM.render(
        <Experience />,
        experience
    )
}



