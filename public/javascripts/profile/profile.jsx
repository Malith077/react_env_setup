'use strict'
import React from 'react';
import TypeWriter from 'typewriter-effect'

class Profile extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="main-div">
        <div className="row">
          <div className="col-lg-4 col-sm-4 first-column">
            <div className="row about-me">
              <div className="container_1">
                <img src="https://media-exp1.licdn.com/dms/image/C4E03AQFRu6jaH7q5-A/profile-displayphoto-shrink_400_400/0?e=1604534400&v=beta&t=cQQ5rdAVYoHqf5yDaWtAItRCg4lNtfQu1qTwtjV7du8" className="img-center" width="100%" alt="logo" />
                <h3 className='bottom-left'>Prabuddha Malith</h3>
              </div>

              <div className="container">
                <div className="info">
                  <ul>
                    <li><span className="material-icons">work</span> Software Engineer</li>
                    <li><span className="material-icons">location_on</span> Melbourne, Australia</li>
                    <li><span className="material-icons">email</span> ggkpmalith@gmail.com</li>
                    <li><span className="material-icons">local_phone</span> +61 452 366 543</li>
                    <li><span className="material-icons">language</span> English</li>
                    <li><span style={{ fontSize: "20px", paddingLeft: "2px", paddingRight: "2px" }}><i class="fab fa-github"></i> </span> <a href="https://github.com/Malith077">Github</a></li>
                    <li><span style={{ fontSize: "20px", paddingLeft: "2px", paddingRight: "2px" }}><i class="fab fa-gitlab"></i> </span> <a href="https://gitlab.com/Malith077">GtiLab</a> </li>
                    <li><span style={{ fontSize: "20px", paddingLeft: "2px", paddingRight: "2px" }}><i class="fab fa-linkedin"></i> </span> <a href="https://linkedin.com/in/prabuddha-malith">LinkedIn</a> </li>
                  </ul>
                  <h4>
                    {/* <h4 className="text-center"> */}
                    <code style={{ color: "#000" }}>
                      <TypeWriter
                        options={{
                          strings: [
                            'while (!(succeed = try()));',
                            'while(!success)<br>{<br> &nbsp;tryAgain();<br><br>&nbsp;if(dead)<br>&nbsp;{<br>&nbsp;&nbsp;&nbsp;break;<br>&nbsp;}   <br>}',
                            'Programming is thinking, not typing.',
                            '\"Code teaches you how to face really big problems\" - Jack Dorsey',
                            '\"Constantly think about how you could be doing things better\" - Elon Musk'
                          ],
                          autoStart: true,
                          loop: true
                        }}
                      />
                    </code>
                  </h4>
                  <hr />
                </div>

                <div>
                  <h3><span class="material-icons">star_rate</span> Skills</h3>
                  <h4><i class="fas fa-terminal"></i> Programming Languages</h4>
                  <ul>
                    <li><p>C++</p></li>

                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '90%' }} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>Javascript</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '80%' }} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">80%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>Python</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '60%' }} aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>C#</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '30%' }} aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">30%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                  </ul>

                  <h4><i class="fas fa-database"></i> Database Management Systems</h4>
                  <ul>

                    <li><p>MySQL</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '90%' }} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>MSSQL</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '90%' }} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>MongoDB</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '85%' }} aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">85%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                  </ul>

                  <hr />
                </div>


                <div>
                  <h4><i class="fas fa-file-code"></i> Framework Expertise</h4>
                  <ul>

                    <li><p>C++ Standard Library</p></li>

                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '90%' }} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>ReactJS, ExpressJS, NodeJS <small>(This website too!)</small></p></li>

                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '85%' }} aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">85%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>React-Native</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '80%' }} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">80%</div>
                          </div>
                        </div>
                      </li>
                    </ul>

                    <li><p>OpenCV</p></li>
                    <ul>
                      <li>
                        <div>
                          <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{ width: '75%' }} aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">75%</div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </ul>

                  <hr />
                </div>

                <div>
                  <h4>Memberships</h4>
                  <ul>
                    <li>Member - Australian Computer Society</li>
                  </ul>
                  <hr />
                </div>

              </div>
            </div>
          </div>


          <div className="col-lg-8  col-sm-8 Second-column">
            <div className="work-experience">
              <h2><span class="material-icons">computer</span> Work Experience</h2>
              <hr />

              <div>
                <ul>
                  <li><h4>C++ Developer - Constraint Technologies International</h4></li>
                  <li><h5>Melbourne</h5></li>
                  <li><span className="badge badge-light">2019/05</span> - <span className="badge badge-light">Current</span></li>
                  <li>
                    <ul>
                      <li>C++/SQL and OpenGL for developing software for Terminal Management and Airport management </li>
                      <li>Agile/Scrum and Waterfall methodologies </li>
                      <li>Developing and Testing Terminal Management System for Hong Kong, Gatwick Airports </li>
                      <li>Design new features on client demands </li>
                    </ul>
                  </li>
                </ul>
                <hr />
              </div>

              <div>

                <ul>
                  <li><h4>React Developer - Adilam Technologies</h4></li>
                  <li><h5>Bayswater, Melbourne</h5></li>
                  <li><span className="badge badge-light">2018/12</span> - <span className="badge badge-light">2019/12</span></li>
                  <li>
                    <ul>
                      <li>JavaScript, MEAN (Mongo, ExpressJS, AngularJS,NodeJs) and MERN (Mongo, ExpressJS, React, NodeJS) along with Redux in order to create an online life story system <a href="https://ourstoryshared.com/">ourstoryshares.com</a> </li>
                      <li>Agile and Waterfall methodologies </li>
                      <li>Design UI/UX designs </li>
                      <li>Develop and test new features on the system </li>
                      <li>Carry out user tests and document findings </li>
                    </ul>
                  </li>
                </ul>
                <hr />
              </div>

              <div>
                <ul>
                  <li><h4>React/MEAN Developer - Fantipper.com</h4></li>
                  <li><h5>Fitzroy, Melbourne</h5></li>
                  <li><span className="badge badge-light">2017/11</span> - <span className="badge badge-light">2019/12</span></li>
                  <li>
                    <ul>
                      <li>MEAN Stack, React and Redux technologies used in creating <a href="https://fantipper.com.au/">fantipper.com</a> </li>
                      <li>React Native is being used for the Fantipper mobile application (Cross-platform) </li>
                      <li>Agile and Waterfall methodologies </li>
                      <li>Implementing a payment platform in order to collect the tips made by users ( Stripe ) </li>
                      <li>UX/UI design and user testing </li>
                    </ul>
                  </li>
                </ul>
                <hr />
              </div>

            </div>

            <div className="education">
              <div>
                <ul>
                  <li><h2><span class="material-icons">school</span> Education </h2></li>
                  <li><h4>Bachelor of Computer Science - Software Developemt</h4></li>
                  <ul>
                    <li>Swinburne University of Technology - Melbourne, Australia</li>
                    <li><span className="badge badge-light">2015/08</span> - <span className="badge badge-light">2019/12</span></li>
                  </ul>
                </ul>
                <hr />
              </div>

              <div>

                <ul>
                  <li><h3><span className="material-icons">code</span> Degree Related Projects</h3></li>
                  <li><h5>Working for ourstoryshared.com at Adilam Technologies as a student for the final year Industry Based Learning (IBL) project units. Achived skills are:</h5></li>
                  <li>
                    <ul>
                      <li>JavaScript, MEAN (Mongo, ExpressJS, AngularJS, NodeJs) and MERN (Mongo, ExpressJS, React, NodeJS) along with Redux in order to create an online life story system ourstoryshares.com</li>
                      <li>Agile and Waterfall methodologies </li>
                      <li>Design UI/UX designs </li>
                      <li>Develop and test new features on the system </li>
                      <li>Carry out user tests and document findings</li>
                    </ul>
                  </li>
                </ul>
              </div>

              <div>
                <ul>
                  <li><h3> <span className="material-icons">emoji_events</span>Awards and Achievements</h3></li>
                  <li><p>Swinburne International Excellence Scholarship (2015) </p></li>
                  <hr/>
                </ul>


              </div>

              <div className='interests'>
                <ul>
                  <li><h4>Other Interests</h4></li>
                  <li>
                    <table class="table table-borderless">
                      <tbody>
                        <tr>
                          <td>
                            <h3><i class="fab fa-aws"></i></h3>
                            <small>AWS</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-angular"></i></h3>
                            <small>Angular JS</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-centos"></i></h3>
                            <small>Centos</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-cc-stripe"></i></h3>
                            <small>Stripe API</small>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3><i class="fab fa-ubuntu"></i></h3>
                            <small>Ubuntu</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-git"></i></h3>
                            <small>Git</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-fedora"></i></h3>
                            <small>Fedora</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-java"></i></h3>
                            <small>Java</small>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3><i class="fab fa-laravel"></i></h3>
                            <small>Laravel</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-linux"></i></h3>
                            <small>Linux</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-npm"></i></h3>
                            <small>NPM</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-redhat"></i></h3>
                            <small>Readhat</small>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3><i class="fab fa-sass"></i></h3>
                            <small>Sass</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-trello"></i></h3>
                            <small>Trello</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-unity"></i></h3>
                            <small>Unity</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-slack"></i></h3>
                            <small>Slack</small>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3><i class="fab fa-vuejs"></i></h3>
                            <small>Vue JS</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-yarn"></i></h3>
                            <small>Yarn</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-jira"></i></h3>
                            <small>Jira</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-jenkins"></i></h3>
                            <small>Jenkins</small>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            <h3><i class="fab fa-digital-ocean"></i></h3>
                            <small>Digital Ocean</small>
                          </td>
                          <td>
                            <h3><i class="fab fa-atlassian"></i></h3>
                            <small>Atlassian</small>
                          </td>
                          <td>
                            <h3><i class="fas fa-microchip"></i></h3>
                            <small>Arduino</small>
                          </td>
                          <td>
                            <h3><i class="fas fa-desktop"></i></h3>
                            <small>PCMR</small>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </li>
                </ul>
              </div>

            </div>


          </div>
        </div>
      </div>
    )
  }
}

export default Profile;